import React, { Component } from "react";
import Body from "../Body/Body";
import Header from "../Header/Header";

export default class BaiTapThucHanhLayout extends Component {
  render() {
    return (
      <div className="">
        <Header />
        <Body />
      </div>
    );
  }
}
